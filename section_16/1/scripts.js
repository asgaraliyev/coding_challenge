    (() => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(async (res) => {
                const {
                    coords: {
                        latitude,
                        longitude
                    }
                } = res
                await fetch(`https://geocode.xyz/${latitude},${longitude}?geoit=json`).then(async (res) => {
                    const {
                        city,
                        country
                    } = await res.json()
                    if (!city) {
                        alert("Slow down pls")
                        changeData({
                            flag: "https://i.pinimg.com/originals/13/9a/19/139a190b930b8efdecfdd5445cae7754.png",
                            name: "Your are in earth",
                            callingCodes:["asd"]
                        })
                        return
                    }
                    console.log(`you are in ${city} ${country}`)
                    getCountryDataAndChange(country)
                }).catch((err) => {
                    alert("Calm down")
                })
            }, (err) => {
                alert("You did not allow we know your geo cordinates.")
            })
        }
    })()
    const flagEl = document.getElementById('image')
    const countryEl = document.getElementById('country')
    const numbercodeEl = document.getElementById('number-code')

    function changeData(data) {
        try {
            if (data?.name && data?.flag && data.callingCodes[0]) {
                flagEl.src = data?.flag
                flagEl.alt = data?.name
                countryEl.innerHTML = data?.name
                numbercodeEl.innerHTML = `+${data.callingCodes[0]}`
            } else {
                throw new Error("Something happened with getting country data,Sorry")
            }
        } catch (error) {
            alert(error.message)
        }
    }
    async function getCountryDataAndChange(country) {

        await fetch(`https://restcountries.eu/rest/v2/name/${country}`).then(async (res) => {
            let data = await res.json()

            data = data[0]
            changeData(data)

        }).catch((err) => {
            alert("Something happened")
        })

    }