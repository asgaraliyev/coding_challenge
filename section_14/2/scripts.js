class Car {
    speed = 0;

    constructor(brand) {
        this.brand = brand
    }
    accelerate = function () {
        this.speed += 10
        this.showSpeed()
    }
    brake = function () {
        this.speed -= 5
        this.showSpeed()
    }
    get speedUs() {
        return this.speed / 1.6
    }
    set speedUs(speedInMile) {
        this.speed = speedInMile * 1.6
    }
    showSpeed = function () {
        console.log(`${this.brand} going at ${this.speed} km/h`)
    }
}
const BmwCar = new Car("BMW")
BmwCar.showSpeed()
BmwCar.accelerate()
BmwCar.accelerate()
BmwCar.showSpeed()
BmwCar.accelerate()
BmwCar.brake()
BmwCar.brake()
BmwCar.brake()
BmwCar.speedUs = 10
console.log(BmwCar.speedUs)