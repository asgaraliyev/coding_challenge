class Car {
    speed = 0;

    constructor(brand) {
        this.brand = brand
    }
    accelerate = function () {
        this.speed += 10
        this.showSpeed()
    }
    brake = function () {
        this.speed -= 5
        this.showSpeed()
    }
    showSpeed = function () {
        console.log(`${this.brand} going at ${this.speed} km/h`)
    }
}
const BmwCar = new Car("BMW")
BmwCar.showSpeed()
BmwCar.accelerate()
BmwCar.accelerate()
BmwCar.showSpeed()
BmwCar.accelerate()
BmwCar.brake()
BmwCar.showSpeed()
BmwCar.brake()