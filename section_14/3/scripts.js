class Car {
    speed = 0;

    constructor(brand) {
        this.brand = brand
    }
    _accelerate = function (value, perBattery) {
        this.speed += value
        this.showSpeed(perBattery)
    }
    _brake = function (value, perBattery) {
        this.speed -= value
        this.showSpeed(perBattery)
    }
    get speedUs() {
        return this.speed / 1.6
    }
    set speedUs(speedInMile) {
        this.speed = speedInMile * 1.6
    }
    showSpeed = function (perBattery) {
        perBattery = parseInt(perBattery)
        console.log(`${this.brand} going at ${this.speed} km/h ${perBattery ? `,with a charge of ${perBattery}%`:""}`)
    }

}

class ElectricCar extends Car {
    constructor(brand, percBtry) {
        super(brand)
        this.percBtry = percBtry
    }
    chargeBattery(chargeBatteryTo) {
        this.percBtry = chargeBatteryTo
    }
    accelerateEV = function () {
        this.chargeBattery(this.percBtry - this.percBtry / 100)
        this._accelerate(20, this.percBtry)
    }
    brakeEv = function () {
        this._brake(10, this.percBtry)
    }
}
const TeslaCar = new ElectricCar("Tesla", 100)
TeslaCar.accelerateEV()
TeslaCar.accelerateEV()
TeslaCar.accelerateEV()
TeslaCar.brakeEv()
// TeslaCar.brakeEv()
// TeslaCar.brakeEv()
TeslaCar.showSpeed()